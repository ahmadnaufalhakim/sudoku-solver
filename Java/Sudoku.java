import java.util.*;
import java.io.*;

public abstract class Sudoku{
    /*** ATTRIBUTES ***/
    protected int size;
    protected int[][] value;
    
    /*** METHODS ***/
    // ctor user-defined
    public Sudoku(int size){
        this.size = size;
        value = new int[size][size];
    }

    /*** MISCELLANEOUS ***/
    // abstract void printGrid()
    public abstract void printGrid();

    // Getter and Setter
    public int getSize(){
        return size;
    }

    public int getValue(int row, int col){
        return value[row][col];
    }

    public void setSize(int size){
        this.size = size;
    }

    public void setValue(int row, int col, int value){
        this.value[row][col] = value;
    }
}