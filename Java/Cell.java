public class Cell{
    /*** ATTRIBUTES ***/
    private int x;
    private int y;

    /*** METHODS ***/
    // ctor default
    public Cell(){
        x = 0;
        y = 0;
    }

    // ctor user-defined
    public Cell(int x, int y){
        this.x = x;
        this.y = y;
    }

    // Getter and Setter
    public int getX(){
        return x;
    }

    public int getY(){
        return y;
    }

    public void setX(int x){
        this.x = x;
    }

    public void setY(int y){
        this.y = y;
    }
}