import java.util.*;
import java.io.*;

public class SmallSudoku extends Sudoku{
    /*** ATTRIBUTES ***/
    private int size;
    private int[][] value;
    
    /*** METHODS ***/
    // ctor user-defined
    public SmallSudoku(int size){
        super(size);
    }

    /*** MISCELLANEOUS ***/
    // void readFile(String fileInp)
    public void readFile(String fileInp){
        try{
			File file = new File(fileInp+".txt");
			FileInputStream fis = new FileInputStream(file);
			ArrayList<Character> arrChar = new ArrayList<Character>();
			while (fis.available() > 0){
				char c = (char)fis.read();
				if ((c=='.' || c=='?' || c=='!') || (c>='0' && c<='9') || (c>='A' && c<='Z') || (c>='a' && c<='z')){
					arrChar.add(c);
				}
			}
			double check1 = Math.sqrt(arrChar.size());
			int check2 = (int)Math.floor(check1);
			if (check1-check2 == 0){
				size = (int)Math.sqrt(arrChar.size());
				value = new int[size][size];
				int k=0;
				for (int i=0; i<size; i++){
					for (int j=0; j<size; j++){
                        if (arrChar.get(k) == '.'){
                            value[i][j] = -1;
                        }
                        else{
                            if (size == 4 || size == 9){
                                // '1' to '9'
                                if (arrChar.get(k) >= '1' && arrChar.get(k) <= '9'){
                                    value[i][j] = Character.getNumericValue(arrChar.get(k));
                                }
                            }
                            else{
                                // '0' to '9'
                                if (arrChar.get(k) >= '0' && arrChar.get(k) <= '9'){
                                    value[i][j] = Character.getNumericValue(arrChar.get(k));
                                }
                                // 'A' to 'Z' == 10 to 35
                                else if (arrChar.get(k) >= 'A' && arrChar.get(k) <= 'Z'){
                                    value[i][j] = (int)(arrChar.get(k)) - 55;
                                }
                                // 'a' to 'z' == 36 to 61
                                else if (arrChar.get(k) >= 'a' && arrChar.get(k) <= 'z'){
                                    value[i][j] = (int)(arrChar.get(k)) - 61;
                                }
                                // '!' == 62 and '?' == 63
                                else{
                                    if (arrChar.get(k) == '!'){
                                        value[i][j] = (int)(arrChar.get(k)) + 29;
                                    }
                                    else if (arrChar.get(k) == '?'){
                                        value[i][j] = (int)(arrChar.get(k));
                                    }
                                }
                            }
                        }
						k++;
					}
				}
			}
			else{
				System.out.println("Invalid file input!\n");
            }
            fis.close();
		}
		catch (FileNotFoundException e){
			System.out.println("File not found!\n");
		}
		catch (IOException e){
			System.out.println("Can't read from file!\n");
		}
    }

    // void printGrid()
    public void printGrid(){
		System.out.printf("\t+");
		for (int k=0; k<(int)Math.sqrt(size); k++){
			for (int l=0; l<((int)(Math.sqrt(size)))*2+1; l++){
				System.out.printf("-");
			}
			System.out.printf("+");
		}
		System.out.println();
        for (int i=0; i<size; i++){
            System.out.printf("\t| ");
            for (int j=0; j<size; j++){
                if (Solver.isUnassigned(this,i,j)){
                    System.out.printf(". ");
                }
                else if (value[i][j] >= 1 && value[i][j] <= 9){
					// print '1' to '9'
					System.out.printf(value[i][j]+" ");
                }

                if (j%((int)(Math.sqrt(size))) == ((int)Math.sqrt(size)-1)){
                    System.out.printf("| ");
                }
                if (((j+1)%size == 0) && (i%((int)(Math.sqrt(size))) == (int)Math.sqrt(size)-1)){
                    System.out.println();
                    System.out.printf("\t+");
                    for (int k=0; k<(int)Math.sqrt(size); k++){
						for (int l=0; l<((int)(Math.sqrt(size)))*2+1; l++){
							System.out.printf("-");
						}
						System.out.printf("+");
					}
                }
            }
            System.out.println();
        }
        System.out.println();
    }
}
