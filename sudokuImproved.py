import numpy as np
import os,time

def readFile(grid, fileInp):
	with open(fileInp+".txt",'r') as f :
		for line in f :
			line = line.rstrip()
			row = []
			for value in line :
				if (value=='.'):
					row.append(0)
				else:
					row.append((int)(value))
			grid.append(row)

def printGrid(grid):
	grid = np.array(grid)
	row,col = grid.shape
	for i in range(row):
		for j in range(col):
			if (isUnassigned(grid,i,j)):
				print(".", end=" ")
			else:
				print(grid[i][j], end=" ")
			if (j==2 or j==5):
				print("|", end=" ")
			elif (j==8 and (i==2 or i==5)):
				print("\n------+-------+------", end=" ")
		print()
	print()

def isInSubGrid(grid, row, col, n):
	for i in [0,1,2]:
		for j in [0,1,2]:
			if (grid[((int)(row/3))*3+i][((int)(col/3))*3+j]==n):
				return True
	return False

def isInRow(grid, row, n):
	for j in range(len(grid)):
		if (grid[row][j]==n):
			return True
	return False

def isInCol(grid, col, n):
	for i in range(len(grid)):
		if (grid[i][col]==n):
			return True
	return False

def isSafe(grid, row, col, n):
	return not isInSubGrid(grid, row, col, n) and not isInRow(grid, row, n) and not isInCol(grid, col, n)

def isUnassigned(grid, row, col):
	return (grid[row][col]==0)

def isGridFull(grid):
	gridTemp = np.array(grid)
	row,col = gridTemp.shape
	for i in range(row):
		for j in range(col):
			if (isUnassigned(grid,i,j)):
				return False
	return True

def searchFirstUnassignedCell(grid, cell):
	if (not isGridFull(grid)):
		gridTemp = np.array(grid)
		row,col = gridTemp.shape
		for i in range(row):
			for j in range(col):
				if (isUnassigned(grid,i,j)):
					cell[0] = i
					cell[1] = j

def countUnassignedCell(grid):
	if (not isGridFull(grid)):
		gridTemp = np.array(grid)
		row,col = gridTemp.shape
		count = 0
		for i in range(row):
			for j in range(col):
				if (isUnassigned(grid,i,j)):
					count += 1
		printGrid(grid)
		return count

def solveObviousCell(grid,arrayCell):
	os.system("cls")
	if (not isGridFull(grid)):
		gridTemp = np.array(grid)
		row,col = gridTemp.shape
		values = [1,2,3,4,5,6,7,8,9]
		for i in range(row):
			for j in range(col):
				candidateSolution = []
				for val in values:
					if (isUnassigned(grid,i,j) and isSafe(grid,i,j,val)):
						candidateSolution.append(val)
				if (len(candidateSolution)==1):
					grid[i][j] = candidateSolution[0]
					arrayCell.append((i,j))
		arrayCell = list(set(arrayCell))
		printGrid(grid)

def solveObviousRow(grid,arrayCell):
	os.system("cls")
	if (not isGridFull(grid)):
		gridTemp = np.array(grid)
		row,col = gridTemp.shape
		values = [1,2,3,4,5,6,7,8,9]
		for i in range(row):
			for val in values:
				candidateSolution = {val:[]}
				for j in range(col):
					if (isUnassigned(grid,i,j) and isSafe(grid,i,j,val)):
						candidateSolution[val].append((i,j))
				if (len(candidateSolution[val])==1):
					grid[candidateSolution[val][0][0]][candidateSolution[val][0][1]] = val
					arrayCell.append((candidateSolution[val][0][0],candidateSolution[val][0][1]))
		arrayCell = list(set(arrayCell))
		printGrid(grid)

def solveObviousCol(grid,arrayCell):
	os.system("cls")
	if (not isGridFull(grid)):
		gridTemp = np.array(grid)
		row,col = gridTemp.shape
		values = [1,2,3,4,5,6,7,8,9]
		for j in range(col):
			for val in values:
				candidateSolution = {val:[]}
				for i in range(row):
					if (isUnassigned(grid,i,j) and isSafe(grid,i,j,val)):
						candidateSolution[val].append((i,j))
				if (len(candidateSolution[val])==1):
					grid[candidateSolution[val][0][0]][candidateSolution[val][0][1]] = val
					arrayCell.append((candidateSolution[val][0][0],candidateSolution[val][0][1]))
		arrayCell = list(set(arrayCell))
		printGrid(grid)

def solveObviousSubGrid(grid,arrayCell):
	os.system("cls")
	if (not isGridFull(grid)):
		values = [1,2,3,4,5,6,7,8,9]
		for iSubGrid in [0,1,2]:
			for jSubGrid in [0,1,2]:
				for val in values:
					candidateSolution = {val:[]}
					for offsetRow in [0,1,2]:
						for offsetCol in [0,1,2]:
							if (isUnassigned(grid,(iSubGrid*3+offsetRow),(jSubGrid*3+offsetCol)) and isSafe(grid,(iSubGrid*3+offsetRow),(jSubGrid*3+offsetCol),val)):
								candidateSolution[val].append((iSubGrid*3+offsetRow,jSubGrid*3+offsetCol))
					if (len(candidateSolution[val])==1):
						grid[candidateSolution[val][0][0]][candidateSolution[val][0][1]] = val
						arrayCell.append((candidateSolution[val][0][0],candidateSolution[val][0][1]))
		arrayCell = list(set(arrayCell))
		printGrid(grid)

def unsolveObvious(grid,arrayCell):
	for cell in arrayCell:
		os.system("cls")
		grid[cell[0]][cell[1]] = 0
		printGrid(grid)

def solveSudokuImprovedBacktrack(grid):
	# Menyimpan cell-cell pada grid Sudoku yang bisa diselesaikan
	# secara manual
	obvCells = []

	# Tambahan UI
	os.system("cls")

	# Untuk mempercepat proses backtracking, program akan menyelesaikan
	# cell-cell pada grid Sudoku yang bisa diselesaikan secara manual,
	# yaitu angka yang hanya bisa di-assign-kan pada 1 cell saja pada
	# subGrid, baris, dan kolom yang bersesuaian dengan cell tersebut,
	# dan cell yang hanya bisa di-assign oleh 1 angka saja
	solveObviousSubGrid(grid,obvCells)
	solveObviousRow(grid,obvCells)
	solveObviousCol(grid,obvCells)
	solveObviousCell(grid,obvCells)
	
	# Jika masih ada cell pada grid Sudoku yang belum di-assign angka,
	# maka proses penyelesaian puzzle Sudoku terus berjalan
	if (isGridFull(grid)):
		return True
	else:
		# List startCell digunakan sebagai tempat untuk menyimpan cell pada
		# grid Sudoku yang belum di-assign
		startCell = [0,0]
		searchFirstUnassignedCell(grid,startCell)
		
		# Menyimpan baris dan kolom ditemukannya cell yang belum di-assign
		row = startCell[0]
		col = startCell[1]
		
		# Mengenumerasi semua kemungkinan angka yang bisa di-assign
		# pada cell tersebut, yaitu 1-9
		for val in range(1,10):
			# Jika angka memenuhi peraturan puzzle Sudoku, maka cell
			# di-assign dengan angka tersebut
			if (isSafe(grid, row, col, val)):
				os.system("cls")
				grid[row][col] = val
				printGrid(grid)
				
				# Menyelesaikan grid Sudoku secara rekursif. Jika puzzle Sudoku
				# belum selesai, maka cell dikosongkan kembali (di-unassign)
				if (solveSudokuImprovedBacktrack(grid)):
					os.system("cls")
					return True
				else:
					os.system("cls")
					grid[row][col] = 0
					printGrid(grid)

		# Jika tidak ada angka yang cocok untuk di-assign pada cell, maka
		# akan melakukan backtrack pada cell sebelumnya
		unsolveObvious(grid,obvCells)
		return False

def main():
	menu = "yes"
	while (menu!="no"):
		os.system("cls")
		grid = []
		gridSolve = []
		f = input(">> Masukkan nama file input : ")
		readFile(grid,f)
		readFile(gridSolve,f)
		
		start = time.time()
		solveSudokuImprovedBacktrack(gridSolve)
		end = time.time()
		
		print("__________________________________________________\n>> Grid Sudoku dengan file input",f,end="")
		print(".txt\n")
		print(">> Banyak cell yang kosong :",countUnassignedCell(grid))
		
		print("__________________________________________________\n>> Solusi Sudoku :\n")
		printGrid(gridSolve)
		print(">> Waktu eksekusi program : ",end-start,"\n")
		print("__________________________________________________\n")
		menu = input(">> Solve again? (yes or no)\n1. yes\n2. no\n>> ")
		while (menu!="yes" and menu!="no"):
			os.system("cls")
			menu = input("Invalid input!\n\nSolve again? (yes or no)\n1. yes\n2. no\n>> ")
		

main()
