import java.util.*;
import java.io.*;

public class Solver{
	/** METHODS **/
	/*** RULES OF SUDOKU ***/
    // static boolean isInRow(Sudoku S, int row, int n)
    public static boolean isInRow(Sudoku S, int row, int n){
        boolean check = false;
        for (int j=0; j<S.size && !check; j++){
            if (S.value[row][j] == n){
                check = true;
            }
        }
        return check;
    }

    // static boolean isInCol(Sudoku S, int col, int n)
    public static boolean isInCol(Sudoku S, int col, int n){
        boolean check = false;
        for (int i=0; i<S.size && !check; i++){
            if (S.value[i][col] == n){
                check = true;
            }
        }
        return check;
    }

    // static boolean isInSubGrid(Sudoku S, int row, int col, int n)
    public static boolean isInSubGrid(Sudoku S, int row, int col, int n){
        boolean check = false;
        int iOffset = (int)(Math.sqrt(S.size));
        int jOffset = iOffset;
        for (int i=0; i<iOffset && !check; i++){
            for (int j=0; j<jOffset && !check; j++){
                if (S.value[(row/iOffset)*iOffset+i][(col/jOffset)*jOffset+j] == n){
                    check = true;
                }
            }
        }
        return check;
    }

    // static boolean isSafe(Sudoku S, int row, int col, int n)
    public static boolean isSafe(Sudoku S, int row, int col, int n){
        return (!isInRow(S,row,n) && !isInCol(S,col,n) && !isInSubGrid(S,row,col,n));
    }

	/*** MISCELLANEOUS ***/
	// static boolean isUnassigned(Sudoku S, int row, int col)
    public static boolean isUnassigned(Sudoku S, int row, int col){
        return (S.value[row][col] == -1);
    }

    // static boolean isGridFull(Sudoku S)
    public static boolean isGridFull(Sudoku S){
        boolean check = true;
        for (int i=0; i<S.size && check; i++){
            for (int j=0; j<S.size && check; j++){
                if (isUnassigned(S,i,j)){
                    check = false;
                }
            }
        }
        return check;
    }

    // static boolean isGridEmpty(Sudoku S)
    public static boolean isGridEmpty(Sudoku S){
        boolean check = true;
        for (int i=0; i<S.size && check; i++){
            for (int j=0; j<S.size && check; j++){
                if (!isUnassigned(S,i,j)){
                    check = false;
                }
            }
        }
        return check;
    }

    // static boolean isGridSafe(Sudoku S)
    public static boolean isGridSafe(Sudoku S){
        boolean check = true;
        for (int i=0; i<S.size && check; i++){
            for (int j=0; j<S.size && check; j++){
                if (!isUnassigned(S,i,j)){
                    if (!isSafe(S,i,j,S.value[i][j])){
                        check = false;
                    }
                }
            }
        }
        return check;
    }

    // static Cell searchFirstUnassignedCell(Sudoku S)
    public static Cell searchFirstUnassignedCell(Sudoku S){
		Cell C = new Cell();
		for (int i=0; i<S.size; i++){
			for (int j=0; j<S.size; j++){
				if (isUnassigned(S,i,j)){
					C.setX(i);
					C.setY(j);
					return C;
				}
			}
		}
		return null;
    }

    // static int countUnassignedCell(Sudoku S)
    public static int countUnassignedCell(Sudoku S){
		int count = 0;
		for (int i=0; i<S.size; i++){
			for (int j=0; j<S.size; j++){
				if (isUnassigned(S,i,j)){
					count++;
				}
			}
		}
		return count;
    }

    






    public static boolean isSizeFromFileValid(String fileName){
        double checker1 = 0;
        int checker2 = 1;
        try{
			File file = new File(fileName+".txt");
			FileInputStream fis = new FileInputStream(file);
			ArrayList<Character> arrChar = new ArrayList<Character>();
			while (fis.available() > 0){
				char c = (char)fis.read();
				if ((c=='.' || c=='?' || c=='!') || (c>='0' && c<='9') || (c>='A' && c<='Z') || (c>='a' && c<='z')){
					arrChar.add(c);
				}
			}
			checker1 = Math.sqrt(arrChar.size());
            checker2 = (int)Math.floor(checker1);
            fis.close();
        }
        catch (FileNotFoundException e){
			System.out.println("File not found!\n");
		}
		catch (IOException e){
			System.out.println("Can't read from file!\n");
		}
		return (checker1-checker2 == 0);
	}
	




	/*** SOLVER ***/
	// static void solveObviousCell(Sudoku S, Set<Cell> obvCells)
    public static void solveObviousCell(Sudoku S, Set<Cell> obvCells){
        if (!isGridFull(S)){
            int[] values = new int[S.size];
            for (int val=0; val<S.size; val++){
                if (S.size == 4 || S.size == 9){
                    values[val] = (val+1);
                }
                else{
                    values[val] = val;
                }
            }
            LinkedList<Cell> obvTemp = new LinkedList<>();
            for (int i=0; i<S.size; i++){
                for (int j=0; j<S.size; j++){
                    LinkedList<Integer> candidateSolution = new LinkedList<Integer>();
                    for (int val: values){
                        if (isUnassigned(S,i,j) && isSafe(S,i,j,val)){
                            candidateSolution.add(val);
                        }
                    }
                    if (candidateSolution.size() == 1){
                        S.value[i][j] = candidateSolution.get(0);
                        obvTemp.add(new Cell(i,j));
                        obvCells.add(new Cell(i,j));
                    }
                }
            }
            if (obvTemp.size() != 0){
                solveObviousCell(S,obvCells);
            }
        }
        
    }

    // static void solveObviousRow(Sudoku S, Set<Cell> obvCells)
    public static void solveObviousRow(Sudoku S, Set<Cell> obvCells){
        if (!isGridFull(S)){
            int[] values = new int[S.size];
            for (int val=0; val<S.size; val++){
                if (S.size == 4 || S.size == 9){
                    values[val] = (val+1);
                }
                else{
                    values[val] = val;
                }
            }
            LinkedList<Cell> obvTemp = new LinkedList<>();
            for (int i=0; i<S.size; i++){
                for (int val: values){
                    Map<Integer, LinkedList<Cell>> candidateSolution = new HashMap<>();
                    LinkedList<Cell> listCellTemp = new LinkedList<>();
                    for (int j=0; j<S.size; j++){
                        Cell cellTemp = new Cell(i,j);
                        if (isUnassigned(S,i,j) && isSafe(S,i,j,val)){
							listCellTemp.add(cellTemp);
                        }
                    }
                    candidateSolution.put(val, listCellTemp);
                    if (candidateSolution.get(val).size() == 1){
                        S.value[candidateSolution.get(val).getFirst().getX()][candidateSolution.get(val).getFirst().getY()] = val;
                        obvTemp.add(candidateSolution.get(val).getFirst());
                        obvCells.add(candidateSolution.get(val).getFirst());
                    }
                }
            }
            if (obvTemp.size() != 0){
                solveObviousRow(S,obvCells);
            }
        }
    }    

    // static void solveObviousCol(Sudoku S, Set<Cell> obvCells)
    public static void solveObviousCol(Sudoku S, Set<Cell> obvCells){
        if (!isGridFull(S)){
            int[] values = new int[S.size];
            for (int val=0; val<S.size; val++){
                if (S.size == 4 || S.size == 9){
                    values[val] = (val+1);
                }
                else{
                    values[val] = val;
                }
            }
            LinkedList<Cell> obvTemp = new LinkedList<>();
            for (int j=0; j<S.size; j++){
                for (int val: values){
                    Map<Integer, LinkedList<Cell>> candidateSolution = new HashMap<>();
                    LinkedList<Cell> listCellTemp = new LinkedList<>();
                    for (int i=0; i<S.size; i++){
                        Cell cellTemp = new Cell(i,j);
                        if (isUnassigned(S,i,j) && isSafe(S,i,j,val)){
                            listCellTemp.add(cellTemp);
                        }
                    }
                    candidateSolution.put(val, listCellTemp);
                    if (candidateSolution.get(val).size() == 1){
                        S.value[candidateSolution.get(val).getFirst().getX()][candidateSolution.get(val).getFirst().getY()] = val;
                        obvTemp.add(candidateSolution.get(val).getFirst());
                        obvCells.add(candidateSolution.get(val).getFirst());
                    }
                }
            }
            if (obvTemp.size() != 0){
                solveObviousCol(S,obvCells);
            }
        }
    }

    // static void solveObviousSubGrid(Sudoku S, Set<Cell> obvCells)
    public static void solveObviousSubGrid(Sudoku S, Set<Cell> obvCells){
        if (!isGridFull(S)){
            int[] values = new int[S.size];
            for (int val=0; val<S.size; val++){
                if (S.size == 4 || S.size == 9){
                    values[val] = (val+1);
                }
                else{
                    values[val] = val;
                }
            }
            LinkedList<Cell> obvTemp = new LinkedList<>();
            int ii = (int)(Math.sqrt(S.size));
            int jj = ii;
            for (int i=0; i<ii; i++){
                for (int j=0; j<jj; j++){
                    for (int val: values){
                        Map<Integer, LinkedList<Cell>> candidateSolution = new HashMap<>();
                        LinkedList<Cell> listCellTemp = new LinkedList<>();
                        for (int offsetI=0; offsetI<ii; offsetI++){
                            for (int offsetJ=0; offsetJ<jj; offsetJ++){
                                Cell cellTemp = new Cell((i*3+offsetI),(j*3+offsetJ));
                                if (isUnassigned(S,(i*3+offsetI),(j*3+offsetJ)) && isSafe(S,(i*3+offsetI),(j*3+offsetJ),val)){
                                    listCellTemp.add(cellTemp);
                                }
                            }
                        }
                        candidateSolution.put(val, listCellTemp);
                        if (candidateSolution.get(val).size() == 1){
                            S.value[candidateSolution.get(val).getFirst().getX()][candidateSolution.get(val).getFirst().getY()] = val;
                            obvTemp.add(candidateSolution.get(val).getFirst());
                            obvCells.add(candidateSolution.get(val).getFirst());
                        }
                    }
                }
            }
            if (obvTemp.size() != 0){
                solveObviousSubGrid(S,obvCells);
            }
        }
    }

    // static void unsolveObvious(Sudoku S, Set<Cell> obvCells)
    public static void unsolveObvious(Sudoku S, Set<Cell> obvCells){
        for (Cell cell: obvCells){
            S.value[cell.getX()][cell.getY()] = -1;
        }
	}
	
	// static boolean solveSmallSudoku(Sudoku S)
    public static boolean solveSmallSudoku(Sudoku S){
        UI.clrscr();
        Set<Cell> obvCells = new LinkedHashSet<>();
        solveObviousSubGrid(S,obvCells);
        solveObviousRow(S,obvCells);
        solveObviousCol(S,obvCells);
        solveObviousCell(S,obvCells);
        if (isGridFull(S)){
            return true;
        }
		Cell startCell = new Cell();
		startCell = searchFirstUnassignedCell(S);
		int row = startCell.getX();
		int col = startCell.getY();

        for (int val=1; val<=S.size; val++){
            if (isSafe(S,row,col,val)){
                UI.clrscr();
                S.value[row][col] = val;
                S.printGrid();
                if (solveSmallSudoku(S)){
                    return true;
                }
                else{
                    UI.clrscr();
                    S.value[row][col] = -1;
                    S.printGrid();
                }
            }
        }
        unsolveObvious(S,obvCells);
        obvCells.clear();
        return false;
    }

    // static boolean solveBigSudoku(Sudoku S)
    public static boolean solveBigSudoku(Sudoku S){
        UI.clrscr();
        Set<Cell> obvCells = new LinkedHashSet<>();
        solveObviousSubGrid(S,obvCells);
        solveObviousRow(S,obvCells);
        solveObviousCol(S,obvCells);
        solveObviousCell(S,obvCells);
        if (isGridFull(S)){
            return true;
        }
        else{
            Cell startCell = new Cell();
            startCell = searchFirstUnassignedCell(S);
            int row = startCell.getX();
            int col = startCell.getY();

            for (int val=0; val<S.size; val++){
                if (isSafe(S,row,col,val)){
                    UI.clrscr();
                    S.value[row][col] = val;
                    S.printGrid();
                    if (solveBigSudoku(S)){
                        return true;
                    }
                    else{
                        UI.clrscr();
                        S.value[row][col] = -1;
                        S.printGrid();
                    }
                }
            }
            unsolveObvious(S,obvCells);
            obvCells.clear();
            return false;
        }
    }

}
