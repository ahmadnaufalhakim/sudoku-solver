import numpy as np
import os,time

def readFile(grid, fileInp):
	with open(fileInp+".txt",'r') as f :
		for line in f :
			line = line.rstrip()
			row = []
			for value in line :
				if (value=='.'):
					row.append(0)
				else:
					row.append((int)(value))
			grid.append(row)

def printGrid(grid):
	grid = np.array(grid)
	row,col = grid.shape
	for i in range(row):
		for j in range(col):
			if (isUnassigned(grid,i,j)):
				print(".", end=" ")
			else:
				print(grid[i][j], end=" ")
			if (j==2 or j==5):
				print("|", end=" ")
			elif (j==8 and (i==2 or i==5)):
				print("\n------+-------+------", end=" ")
		print()
	print()

def isInSubGrid(grid, row, col, n):
	for i in [0,1,2]:
		for j in [0,1,2]:
			if (grid[((int)(row/3))*3+i][((int)(col/3))*3+j]==n):
				return True
	return False

def isInRow(grid, row, n):
	for j in range(len(grid)):
		if (grid[row][j]==n):
			return True
	return False

def isInCol(grid, col, n):
	for i in range(len(grid)):
		if (grid[i][col]==n):
			return True
	return False

def isSafe(grid, row, col, n):
	return not isInSubGrid(grid, row, col, n) and not isInRow(grid, row, n) and not isInCol(grid, col, n)

def isUnassigned(grid, row, col):
	return (grid[row][col]==0)

def isGridFull(grid):
	gridTemp = np.array(grid)
	row,col = gridTemp.shape
	for i in range(row):
		for j in range(col):
			if (isUnassigned(grid,i,j)):
				return False
	return True

def searchFirstUnassignedCell(grid, cell):
	if (not isGridFull(grid)):
		gridTemp = np.array(grid)
		row,col = gridTemp.shape
		for i in range(row):
			for j in range(col):
				if (isUnassigned(grid,i,j)):
					cell[0] = i
					cell[1] = j

def countUnassignedCell(grid):
	if (not isGridFull(grid)):
		gridTemp = np.array(grid)
		row,col = gridTemp.shape
		count = 0
		for i in range(row):
			for j in range(col):
				if (isUnassigned(grid,i,j)):
					count += 1
		return count

def solveSudokuBacktrack(grid):
	# Jika masih ada cell pada grid Sudoku yang belum di-assign angka,
	# maka proses penyelesaian puzzle Sudoku terus berjalan
	if (isGridFull(grid)):
		return True
	else:
		# List startCell digunakan sebagai tempat untuk menyimpan cell pada
		# grid Sudoku yang belum di-assign
		startCell = [0,0]
		searchFirstUnassignedCell(grid,startCell)
		
		# Menyimpan baris dan kolom ditemukannya cell yang belum di-assign
		row = startCell[0]
		col = startCell[1]
		
		# Mengenumerasi semua kemungkinan angka yang bisa di-assign
		# pada cell tersebut, yaitu 1-9
		for val in range(1,10):
			# Jika angka memenuhi peraturan puzzle Sudoku, maka cell
			# di-assign dengan angka tersebut
			if (isSafe(grid, row, col, val)):
				grid[row][col] = val

				# Menyelesaikan grid Sudoku secara rekursif. Jika puzzle Sudoku
				# belum selesai, maka cell dikosongkan kembali (di-unassign)
				if (solveSudokuBacktrack(grid)):
					return True
				else:
					grid[row][col] = 0

		# Jika tidak ada angka yang cocok untuk di-assign pada cell, maka
		# akan melakukan backtrack pada cell sebelumnya
		return False

def main():
	menu = "yes"
	while (menu!="no"):
		grid = []
		f = input(">> Masukkan nama file input : ")
		readFile(grid,f)
		print("__________________________________________________\nGrid Sudoku dengan file input",f,end="")
		print(".txt\n")
		printGrid(grid)
		print("Banyak cell yang kosong :",countUnassignedCell(grid))
		
		start = time.time()
		solveSudokuBacktrack(grid)
		end = time.time()
		
		print("__________________________________________________\nSolusi Sudoku :\n")
		printGrid(grid)
		print("Waktu eksekusi program : ",end-start,"\n")
		print("__________________________________________________\n")
		menu = input("Solve again?\n1. yes\n2. no\n>> ")

main()
